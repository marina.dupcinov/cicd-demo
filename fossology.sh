#!/bin/bash
# Running Fossology check on changed files in the recent commit

# find files that have been modified and create tar.gz file
project=helloWorld
tar -czvf ${project}.tar.gz $(git diff --name-only HEAD..HEAD~1)

echo "Check FOSS for ${project}.tar.gz"

# create a new token
# must last for at least a day, so read the tomorrow's date
# example response from Fossology server: 
# {"Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3QiLCJhdWQiOiJsb2NhbGhvc3QiLCJleHAiOjE1NjQ4NzY3OTksIm5iZiI6MTU2NDcwNDAwMCwianRpIjoiTXk0eiIsInNjb3BlIjoid3JpdGUifQ.fVN-SwkkqEQtIirebpclf3_ZsarKKGNPj_hXF22djek"}

tokenExpiryDate=$(date -d tomorrow +%F)

# simple usage of a variable in the --data part of json doesn't work
# need to use this jq command in order to modify the body
# jq needs to be installed first with 'sudo apt-get install jq'

request_body=$(jq -n --arg edate "$tokenExpiryDate" '
{
    username: "fossy",
    password: "fossy",
    token_name: "ciTest5",
    token_scope: "write",
    token_expire: $edate
}')
set -x #start debug

#token=$(curl -k -s -S -X POST http://fossology:80/repo/api/v1/tokens \
token=$(curl -k -s -S -X POST http://localhost:8081/repo/api/v1/tokens \
    -H "Content-Type: application/json" \
      --data "$request_body" | grep -o '{"Author.*[^\}]\+'| gawk -F'"' '{print $4}')
      
echo "Token : $token"      
# upload file
folderId=1
fileinput=$(echo "fileInput=@""\"${project}.tar.gz\";type=application/octet-stream")

#uploadid=$(curl -k -s -S -X POST http://fossology:80/repo/api/v1/uploads \
uploadid=$(curl -k -s -S -X POST http://localhost:8081/repo/api/v1/uploads \
   -H "folderId: $folderId" \
   -H 'uploadDescription: created by REST' \
   -H 'public: public' \
   -H 'Content-Type: multipart/form-data' \
   -F "${fileinput}" \
   -H "Authorization: ${token}" | grep -o '{\"code.*message":[^\}]\+'| gawk -F':|,' '$2 == 201 {print $4} $2 != 201 {print "-1"}') 

echo "Upload finished with $uploadId"
if [ $uploadid == -1 ] 
then
    echo "Failed to upload file $i" 
    exit 1
fi

# wait for the file to get uploaded, otherwise cannot find the uploaded file
sleep 3

# trigger analysis jobs
#response=$(curl -k -s -S -X POST http://fossology:80/repo/api/v1/jobs \
response=$(curl -k -s -S -X POST http://localhost:8081/repo/api/v1/jobs \
    -H "folderId: $folderId" \
    -H "uploadId: $uploadid" \
    -H "Authorization: ${token}" \
    -H 'Content-Type: application/json' \
      --data '{
        "analysis": {
          "bucket": true,
          "copyright_email_author": true,
          "ecc": true, "keyword": true,
          "mime": true,
          "monk": true,
          "nomos": true,
          "package": true
        },
        "decider": {
          "nomos_monk": true,
          "bulk_reused": true,
          "new_scanner": true
        }
      }' | grep -o '{\"code.*message":[^\}]\+' | gawk -F':|,' '{print $2}')
echo "Analysis job finished with code $response"

#Trigger report generation
if [ $response != 201 ] 
then
    echo "Failed to start analysis for file $i" 
    exit 1
fi
#response=$(curl -k -s -S -X GET http://fossology:80/repo/api/v1/report \
response=$(curl -k -s -S -X GET http://localhost:8081/repo/api/v1/report \
    -H "uploadId: ${uploadid}" -H 'reportFormat: spdx2' \
    -H "Authorization: ${token}" | grep -o '{\"code.*message":[^}]\+'| gawk -F':|,' '{print $2,$4}')

# read the reportId from the response. The example of the response is: {"code":201,"message":"localhost\/repo\/api\/v1\/report\/6","type":"INFO"}
reportId=$(echo $response|gawk -F"/|\"" '{print $7}')

if [ $(echo $response|gawk '{print $1}') != 201 ] 
then
    echo "Failed to trigger report generation for file $i" 
    exit 1
fi

# Download generated report
# need to wait for report to get generated
echo "Report generation triggered with response $response"
echo "Wait 300s for report http://fossology:80/repo/api/v1/report/${reportId} to be generated"
sleep 20
#curl -k -s -S -X GET http://fossology:80/repo/api/v1/report/${reportId} \
curl -k -s -S -X GET http://localhost:8081/repo/api/v1/report/${reportId} \
    -H "Authorization: ${token}" \
    -H 'accept: text/plain' > report.rdf.xml


