# cicd-demo

Welcome to the CICD Demo! This repository will be an area for demonstrating (at a relatively smaller scale) what it looks like to see a CI/CD setup in action.

This page will serve as a guide for how the repository should be configured. It will start from a point when the repo just has source code, and then walk through configuring a full pipeline.

## Overview

First things first - let's cover basic tenants of solid CI/CD and provide some resources to get started. For context, the main goal here is to create repeatable processes that enable teams to iterate frequently on projects to ensure product quality and response to changing criteria nimbly. 

Here are some DevOps staple concepts/principles:
  - Shift left: the more frequently developers can commit code, receive feedback, and deploy, the better the quality of the product
  - Consistency is key: take advantage of service isolation tools to package all dependencies in one bundle that can be deployed to any instance, regardless of host operating system or local packages needing to be installed
  - More commits = smaller commits = smaller changes = easier deployments: this means that if we can keep changes focused and atomic (and frequent), we can avoid doing large code merges and deployments that are hard to support (and debug)
  - Aim for boring: processes should be so repeatable and consistent that building, testing, and deploying happen the same way every time, and can be used both on local developer machines as well as in production
  
And here are some resources that are a good reference as you develop resources for teams:
  - [The 12-factor App](https://12factor.net/) - this is a great reference for architecting an application to follow general best practices
  - [Docker - getting started](https://docs.docker.com/get-started/) - this is an awesome tutorial that takes you from a single Dockerfile to running a load-balanced app on a swarm (all on your local machine), and is a great foundation for understanding Docker
  - [Gitlab CI configuration](https://docs.gitlab.com/ee/ci/yaml/) - this is the main reference document for the building blocks you can use in your Gitlab pipeline files
  - [Gitlab CI templates](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/lib/gitlab/ci/templates) - this page has a collection of pipeline templates for different flavors of source code

On the personal side, here are some core qualities of good DevOps evangelists:
  * First and foremost, a strong grasp of Linux core concepts
  * Desire and ability to constantly learn new tools
  * Ablility to communicate ideas and requirements between teams
  * Ability to design well-architected solutions based on business value
  * Ability to listen well to the needs of stakeholders and translate them into well-documented technical solutions
  * Focus on creating fault-tolerant, stable, and easily repeatable solutions that can be supported by others with basic training and documentation (the concept of making yourself replaceable)
  * At least a basic understanding of software development concepts

One note - in this tutorial we will assume you're pushing directly to the **master** branch, but the real magic comes in the collaborative environment when everyone pulls a feature branch off of that **master** branch to test their changes before merging. The CI pipelines can do special steps on those merge requests to give feedback that the changes they're making have been built and tested so we assure that the **master** branch never has broken code.

Enough talk - let's build.

## Preparation

You'll probably want to clone this repository to your computer so you can develop in your own editor. You can use either method to clone:

  * HTTPS (requires username + personal access token generated on Gitlab)
  * SSH (requires uploading your SSH public key to Gitlab)

Whichever you choose, the instructions are easy to find on Google/on Gitlab's docs online.

Also, if you have Docker installed locally, it'll be nice to test things out locally before commiting to Gitlab.

## Part 1: Source code

Although not all CI/CD engineers are developers by trade, it's important to know the basics of the source code you're automating. Things like the language, build tools, and tests involved are critical parts you'll have to consider when creating the build environment and pipeline.

In this section, we'll be adding a Dockerfile to the repository that will define our build environment as code. It needs to:

  1. Start with a base image that has Python installed
  2. Copy in the code
  3. Install any dependencies defined in requirements.txt (hint: use `pip`)
  4. Run the application (hint: use the `ENTRYPOINT` & `CMD` directives for this so those steps only happen when the container is run)

Let's get to the to do list:

  - [ ] Fork this repository into a repository under your account
  - [ ] Clone that repository to your computer
  - [ ] Check out the source code in **app.py** to get an idea of what's happening
  - [ ] Add a **Dockerfile** to the root of the repository that accomplishes the steps above (Google can help here)
  - [ ] Test your Dockerfile
    * `docker build -t hello-world:test .`
    * `docker run -d -p 5000:5000 hello-world:test`
    * Go to your browser: **http://localhost:5000**
    * You should see the application running!

## Part 2: Continuous Integration

Part 1 was a big step - we can now build and run the application on any machine with Docker installed because we've defined eveything we need within the Dockerfile. The best part is it's stored alongside the source code, and can be peer reviewed and evolve along with the source code it supports.

Next up is creating a pipeline around this application. In the concept of continuous integration, frequent commits to a trunk (often the **master** branch by default) trigger a pipeline that will generally build, test, and deploy the application.

Because we're following a best practice of bundling a Dockerfile with our application, we just need this pipeline to:

  1. Build the Docker image for us like we did by hand in part 1
  2. Upload the image to the Docker image registry that's attached to the hello-world repository (via the **Registry** section on the left panel)

Once the image is available in the Docker image registry, it can be picked up and used by container orchestrators like Docker Compose, Docker Swarm, and Kubernetes. For bonus points, you can add more stages in this pipeline like testing.

Here's the to do list:

  - [ ] In the same repository (hello-world), add a file called **.gitlab-ci.yml** to the root of the repository (same location as the **Dockerfile**)
  - [ ] Use the Gitlab CI configuration guide linked in the Overview section to get an idea of what you can add to this file 
  - [ ] Also refer to the Gitlab CI templates linked above for inspiration here (hint - there's a Docker pipeline in that repository which will get you started)
  - [ ] Push your **.gitlab-ci.yml** file to the repository and watch the pipeline trigger automatically
  - [ ] Click on the left hand panel: CI / CD -> Pipelines and click on the pipeline that's running for your recent commit. You can click into the job to see the console output to make sure everything is working. If it did, you can click **Registry** on the left panel and see your application image stored there.

Now whenever a change is made to the source code, Gitlab will automatically notice the change and will rebuild our application, package it into a Docker image, and store it in the registry for future use - all without any manual intervention. 

## Part 3: Introduction to Container Orchestration

With that Docker image sitting in the registry waiting to be used, we need some orchestration mechanism that will take the image and run it with some additional configuration options like replica count, memory usage, ports, volumes, etc.

For learning purposes, we're going to use Docker Compose for this. As always, Docker has great documentation on it [here](https://docs.docker.com/compose/overview/).

Here's the to do list:

  - [ ] In the root of the repository, add a file called **docker-compose.yml**
  - [ ] Using the online documentation, fill out this file with the following settings:
    * image: the link to the image in your app's image repository
    * ports: open port 5000 from the container to port 5000 on the host
  - [ ] Test by running `docker-compose up -d` locally
    * This one command replaces the two build and run commands you used earlier
    * It's inherently referring to the **docker-compose.yml** file in the same directory
    * The `-d` flag is telling it to start in detatched mode so it doesn't tie up your terminal
    * Once again, go to **http://localhost:5000** and see the web app up and running!
  - [ ] Push your **docker-compose.yml** file to the repository
  - [ ] To test everything end-to-end, try making a source code change in **app.py** to say something different in the default app route
    * After committing your change, watch the pipeline run through and finish uploading the image to the image registry
    * Grab the new verison of your image by running `docker pull <link to your image>`
    * Then, run `docker-compose up -d` once more and see your custom message at **http://localhost:5000**
    * You can shut it down with `docker-compose down`

At this point, you have shown that a developer could focus on making code changes, and the rest of the pipeline is automated. The developer can even test their changes locally by running one command with Docker Compose, and that same image will be used in production. Consistency achieved.

## [Extra credit] Part 4: Continuous Deployment

The concept of Continuous Deployment is to automatically deploy our containerized application to our environment(s) assuming the previous steps in the pipeline have completed successfully.

This step is extra credit because it will require some hardware to run our application. If you do have access to dedicated hardware, I recommend adding your instance as a Gitlab runner. Instructions to do that are [here](https://docs.gitlab.com/runner/install/) as well as on the left under Settings -> CI / CD -> Runners ("Set up a specific Runner manually").

Once your runner is added, we can add a final stage to your **.gitlab-ci.yml** file for deployment (after the stage that uploads the image to the registry).

Here's the to do list:

  - [ ] Add a job at the end of your **.gitlab-ci.yml** called "deploy" or something similar
  - [ ] In the `script` portion of this job, you will call the same command from Part 3: `docker-compose up -d`
    * Note: best practice is to tag each version of the image you upload, and refer to that image tag/version in your Docker Compose file to ensure you're using a specific version each time you deploy
    * However, if you're leaving off the tag or just using "latest" by default, then your pipeline should call `docker pull <image link>` before calling docker-compose to ensure it has the latest version of your image
  - [ ] Push your modified pipeline file to your repository and watch the the pipeline kick off
    * If all goes well, you should be able to go to **http://your.server.ip:5000** and see your application running!

You have now rounded out your pipeline - simply editing the source code and commiting back to Gitlab will trigger the pipeline, and in a few moments your changes will be visible through your browser on a real server! Continuous deployment: check.

## Summary

So that's it! Starting from source code, we defined the build environment with all dependencies as code, added a pipeline to package the code in its environment into a Docker image, and stored it in our repository's Docker registry. 

For testing purposes, we used Docker Compose to take that image and run it locally. In production, we would use container orchestration like Docker Swarm or Kubernetes to take advantage of auto-scaling and other awesome features.

If you want to learn more, the next logical step is to get into Kubernetes for big time container orchestration. You can find their documentation [here](https://kubernetes.io/docs/home/?path=users&persona=app-developer&level=foundational). Enjoy!