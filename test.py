import unittest
from app import hello

class HelloWorldTestCase(unittest.TestCase):
    """Tests for `app.py`."""

    def test_is_output_hw(self):
        """Is the output of your Python Application what you expect?"""
#        self.assertTrue(hello() == "Hello World from Pipelines!")
        self.assertTrue(hello() == "Hello from the Python web application in Docker!")

if __name__ == '__main__':
    unittest.main()
